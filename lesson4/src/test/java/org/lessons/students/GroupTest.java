package org.lessons.students;

import org.junit.jupiter.api.*;

import java.util.ArrayList;

import static org.mockito.Mockito.*;

public class GroupTest {
    private Group group;
    private Student student1;
    private Student student2;
    private Student student3;

    @BeforeAll
    //@BeforeClass
    static void beforeAll() {
        System.out.println("First");
    }

    @BeforeEach
    //@Before in JUnit 4
    public void init() {
        System.out.println("init method");

        //region mock
        student1 = mock(Student.class);
        student2 = mock(Student.class);
        student3 = mock(Student.class);
        //endregion

        when(student1.getYearOfEducation()).thenReturn(1);
        when(student2.getYearOfEducation()).thenReturn(2);
        when(student3.getYearOfEducation()).thenReturn(4);

        group = new Group();

        group.add(student1);
        group.add(student2);
        group.add(student3);
    }

    @AfterEach
    public void after() {
        System.out.println("After");
    }

    @Test
    void shouldCallGetYearIfAverageYearIsCalledTest() {
        group.getAverageYearOfEducation();

        verify(student1).getYearOfEducation();
        verify(student2).getYearOfEducation();
        verify(student3).getYearOfEducation();
    }

    @Test
    void getAverageYearOfEducationTest() {
        Assertions.assertEquals(2, group.getAverageYearOfEducation());
    }

    @Test
    @Disabled
    void getNumberOfStudentsWithScholarshipTest() {
        Assertions.assertEquals(2, group.getNumberOfStudentsWithScholarship());
    }
}
