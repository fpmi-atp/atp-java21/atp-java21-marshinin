package org.lessons.students;

import java.util.ArrayList;

public class Group {
    private ArrayList<Student> students;

    public Group(ArrayList<Student> students) {
        this.students = students;
    }

    public Group() {
        students = new ArrayList<>();
    }

    public void add(Student student) {
        students.add(student);
    }

    public int getAverageYearOfEducation() {
        int result = 0;
        for (Student student : students) {
            result += student.getYearOfEducation();
        }
        return result / students.size();
    }

    public int getNumberOfStudentsWithScholarship(){
        int result = 0;
        for (Student student : students) {
            if (student.isHasScholarship()) {
                result++;
            }
        }
        return result;
    }

}
