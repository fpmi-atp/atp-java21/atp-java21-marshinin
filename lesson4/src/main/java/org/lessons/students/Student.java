package org.lessons.students;

import lombok.Data;
import org.lessons.exception.MyException;

import java.io.IOException;

@Data
public class Student {
    private final String name;
    private int yearOfEducation;
    private boolean hasScholarship;
    private Department department;

    public Student(String name, int yearOfEducation, boolean hasScholarship, Department department) {
        this.name = name;
        this.yearOfEducation = yearOfEducation;
        this.hasScholarship = hasScholarship;
        this.department = department;
    }

    public void setYearOfEducation(int yearOfEducation) throws MyException{
        if (yearOfEducation < 1 || yearOfEducation > 10) {
            throw new MyException(new IllegalArgumentException("Wrong year of education"));
        }

        this.yearOfEducation = yearOfEducation;
    }

    //Never do like that
    public boolean isStringInteger(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
