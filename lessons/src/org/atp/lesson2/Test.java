package org.atp.lesson2;

import static java.lang.Math.*;

public class Test {
    public static void main(String[] args) {
        double x = 0.5;
        double y = sin(x) * sin(x) + cos(x) * cos(x);
        System.out.println(y);
    }
}
