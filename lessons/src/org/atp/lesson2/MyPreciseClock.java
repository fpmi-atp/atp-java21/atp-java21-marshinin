package org.atp.lesson2;

public class MyPreciseClock extends MyClock {
    private int seconds;

    public MyPreciseClock(int hours, int minutes, int seconds) {
        super(hours, minutes);
        this.seconds = seconds;
    }

    public int getSeconds() {
        return seconds;
    }

    public void nextSecond() {
        seconds++;
        if (seconds >= 60) {
            seconds = 0;
            nextMinute();
        }
    }

    @Override
    public String toString() {
        return "MyPreciseClock{" +
                getHours() + ":" +
                getMinutes() + ":" +
                "seconds=" + seconds +
                '}';
    }
}
