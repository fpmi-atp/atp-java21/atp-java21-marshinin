package org.atp.lesson2;

public interface Walkable {
    void walk();

    //Do not do like that!
    void run();
}
