package org.atp.lesson2;

public abstract class MyClock {
    private int hours;
    private int minutes;

    public MyClock(int hours, int minutes) {
        this.hours = hours;
        this.minutes = minutes;
    }

    public MyClock() {
        this(0, 0);
    }

    public int getHours() {
        return hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void nextMinute() {
        minutes++;
        if (minutes >= 60) {
            minutes = 0;
            hours++;
            if (hours >= 24) {
                hours = 0;
            }
        }
    }

    @Override
    public String toString() {
        return "MyClock{" +
                "hours=" + hours +
                ", minutes=" + minutes +
                '}';
    }
}
