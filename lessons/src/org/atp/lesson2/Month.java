package org.atp.lesson2;

public enum Month {
    JANUARY, FEBRUARY
}

class EnumExample {
    void setDate(int day, Month month) {
        //...
    }

    void someMethod() {
        setDate(23, Month.FEBRUARY);
    }
}
