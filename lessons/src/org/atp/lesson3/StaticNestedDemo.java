package org.atp.lesson3;

import java.util.Timer;
import java.util.TimerTask;

public class StaticNestedDemo extends Main {
    private String hello = "Hello world!!!";

    static class HelloTimerTask extends TimerTask {
        private StaticNestedDemo outer;

        public HelloTimerTask(StaticNestedDemo outer) {
            this.outer = outer;
        }

        @Override
        public void run() {
            System.out.println(outer.hello);
        }
    }

    public void helloWorld() {
        TimerTask timerTask = new HelloTimerTask(this);
        Timer timer = new Timer();
        timer.schedule(timerTask, 1000, 1000);
    }
}
