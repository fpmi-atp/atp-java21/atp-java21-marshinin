package org.atp.lesson3;

import java.util.Timer;
import java.util.TimerTask;

public class LocalClassDemo {
    private String hello = "Hello, ";

    public void hello(final String who) {
        final String mr = "Mr.";

        class HelloTimerTask extends TimerTask {

            @Override
            public void run() {
                System.out.println(hello + mr + who);
            }
        }

        TimerTask timerTask = new HelloTimerTask();
        Timer timer = new Timer();
        timer.schedule(timerTask, 1000, 1000);
    }
}
