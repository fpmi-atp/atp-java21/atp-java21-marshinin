package org.atp.lesson3;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class RandomAccessDemo {
    public static void main(String[] args) {
        List<String> list1 = new ArrayList<>();
        List<String> list2 = new LinkedList<>();

        for (int i = 0; i < 100000; i++) {
            list1.add("" + i);
            list2.add("" + i);
        }

        long t0 = System.nanoTime();
        for (int i = 0; i < list2.size(); i++) {
            System.out.println(list1.get(new Random().nextInt(list2.size())));
        }
        long t1 = System.nanoTime();
        for (int i = 0; i < list1.size(); i++) {
            System.out.println(list1.get(new Random().nextInt(list1.size())));
        }
        long t2 = System.nanoTime();

        System.out.println("First " + (t1 - t0) + "; Second " + (t2 - t1));

    }
}
