package org.atp.lesson3;

import java.util.Timer;
import java.util.TimerTask;

public class InnerClassDemo {
    private String hello = "Hello world!!!";

    class HelloTimerTask extends TimerTask {

        @Override
        public void run() {
            System.out.println(hello);
        }
    }

    public void helloWorld() {
        TimerTask timerTask = new HelloTimerTask();
        Timer timer = new Timer();
        timer.schedule(timerTask, 1000, 1000);
    }
}
