package org.atp.lesson3;

import java.util.ArrayList;
import java.util.List;

public class ArrayListDemo {
    public static void main(String[] args) {
        List<String> list = new ArrayList();
        list.add("one");
        list.add("two");
        list.add("one");

        for (String s : list) {
            System.out.println(s.length());
        }
    }
}
