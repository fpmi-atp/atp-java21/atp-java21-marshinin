package org.atp.lesson3;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class HashSetDemo {
    public static void main(String[] args) {
        Set set = new HashSet();

        set.add("one");
        set.add("two");
        set.add("three");
        set.add("one");
        set.add("two");
        set.add(1);
        set.add(null);

        System.out.println(set);
    }
}
