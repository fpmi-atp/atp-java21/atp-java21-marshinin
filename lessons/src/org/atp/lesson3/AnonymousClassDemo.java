package org.atp.lesson3;

import java.util.*;

public class AnonymousClassDemo {
    private String hello = "Hello, %s%s!";

    public void hello(final String who){
        final String mr = "Mr.";
        
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                System.out.printf(hello, mr, who);
            }
        };
        Timer timer = new Timer();
        timer.schedule(task, 1000, 1000);
    }
}
