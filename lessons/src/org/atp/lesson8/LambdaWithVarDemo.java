package org.atp.lesson8;

import java.util.concurrent.atomic.AtomicInteger;

public class LambdaWithVarDemo {
    private static int x = 10;
    private static int y = 20;

    public static void main(String[] args) {
        SuperSimpleCalculator calculator = () -> {
            x = 30;
            return x + y;
        };

        System.out.println(calculator.calculate());
        System.out.println(x);

//        int n = 40;
        AtomicInteger n = new AtomicInteger(40);
        int m = 50;

        SuperSimpleCalculator calculator1 = () -> {
//            n = 60;
            n.set(60);
            return n.get() + m;
        };

        System.out.println(calculator1.calculate());
        System.out.println(n.get());
    }
}
