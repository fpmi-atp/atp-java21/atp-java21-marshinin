package org.atp.lesson8;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamDemo {
    public static void main(String[] args) {
        int[] arr = {50, 60, 70, 80};
        int count = 0;
        for (int x : arr) {
            if (x <= 50) continue;
            count++;
            if (count > 2) break;
            System.out.println(x);
        }

        IntStream.of(50, 60, 70, 80).filter(x -> x > 50).map(x -> x + 10).limit(2).forEach(System.out::println);

        Stream.empty();
        List list = new ArrayList();
        list.stream();
        Stream.of();
    }
}
