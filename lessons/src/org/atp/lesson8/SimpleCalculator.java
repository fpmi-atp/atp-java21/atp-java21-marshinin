package org.atp.lesson8;

@FunctionalInterface
public interface SimpleCalculator {
    int calculate(int x);
}
