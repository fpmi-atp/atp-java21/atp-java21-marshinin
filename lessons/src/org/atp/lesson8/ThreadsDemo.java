package org.atp.lesson8;

public class ThreadsDemo {
    public static void main(String[] args) throws InterruptedException {
        Thread threadObject = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!Thread.interrupted()){
                    try {
                        System.out.println(Thread.currentThread().getName());
                        Thread.sleep(1_000);
                    } catch (InterruptedException e) {
                        System.out.println("Interrupted");
                        Thread.currentThread().interrupt();
                    }
                }
            }
        });

        threadObject.setDaemon(true);
        threadObject.setPriority(Thread.MAX_PRIORITY);
        threadObject.getState();
        threadObject.start();
        Thread.sleep(5_000);
        threadObject.interrupt();

        threadObject.join();
    }
}
