package org.atp.lesson8;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MapStreamDemo {
    public static void main(String[] args) {
        String[] array = {"FSMSRFO", "SRPgmposrm"};
        Stream<String> stream = Arrays.stream(array);
        List<String> strings = stream.map(s -> s.split(""))
                .flatMap(Arrays::stream).distinct()
                .collect(Collectors.toList());
    }
}
