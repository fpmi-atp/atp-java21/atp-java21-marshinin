package org.atp.lesson8;

@FunctionalInterface
public interface SuperSimpleCalculator {
    int calculate();
}
