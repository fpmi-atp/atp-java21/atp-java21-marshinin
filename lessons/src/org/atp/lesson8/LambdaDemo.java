package org.atp.lesson8;

public class LambdaDemo {
    public static void main(String[] args) {
        Calculator calculator = (x, y) -> x + y;
        Calculator calculator1 = (x, y) -> x * y;

        SimpleCalculator simpleCalculator = x -> {
            int y = x * x;
            return y;
        };

        // (параметры) -> {метод}
        System.out.println(calculator.calculate(10, 20));
        System.out.println(calculator1.calculate(10, 20));

        System.out.println(simpleCalculator.calculate(10));
    }
}
