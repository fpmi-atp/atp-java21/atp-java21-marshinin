package org.atp.lesson8;

@FunctionalInterface
public interface Calculator {
    int calculate(int x, int y);
}
