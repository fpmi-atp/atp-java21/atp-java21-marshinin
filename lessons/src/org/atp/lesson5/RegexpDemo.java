package org.atp.lesson5;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexpDemo {
    public static void main(String[] args) {
        String text = "Егор Алла Александр";

        Pattern pattern1 = Pattern.compile("[А-Яа-я].+[А-Яа-я]");
        Pattern pattern2 = Pattern.compile("[А-Яа-я].+?[А-Яа-я]");
        Pattern pattern3 = Pattern.compile("[А-Яа-я].++[А-Яа-я]");
        Matcher matcher = pattern3.matcher(text);

        while (matcher.find()) {
            System.out.println(text.substring(matcher.start(), matcher.end()));
        }
    }
}
