package org.atp.lesson1.students;

public class Group {
    private Student[] students;

    public Group(Student[] students) {
        this.students = students;
    }

    public void add(Student student) {
        Student[] actual = new Student[students.length + 1];
        System.arraycopy(students, 0, actual, 0, students.length);
        actual[actual.length - 1] = student;
        students = actual;
    }

}
