package org.atp.lesson1.students;

import java.util.Objects;

public class Student {
    private final String name;
    private int yearOfEducation;
    private boolean hasScholarship;
    private Department department;

    public Student(String name, int yearOfEducation, boolean hasScholarship, Department department) {
        this.name = name;
        this.yearOfEducation = yearOfEducation;
        this.hasScholarship = hasScholarship;
        this.department = department;
    }

    public Student(String name, int yearOfEducation, Department department) {
        this.name = name;
        this.yearOfEducation = yearOfEducation;
        this.department = department;
    }

    public String getName() {
        return name;
    }

    public int getYearOfEducation() {
        return yearOfEducation;
    }

    public void setYearOfEducation(int yearOfEducation) {
        if (yearOfEducation > 0) {
            this.yearOfEducation = yearOfEducation;
        }
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", yearOfEducation=" + yearOfEducation +
                ", hasScholarship=" + hasScholarship +
                ", department=" + department +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return yearOfEducation == student.yearOfEducation && hasScholarship == student.hasScholarship && name.equals(student.name) && department == student.department;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, yearOfEducation, hasScholarship, department);
    }

    public boolean hasScholarship() {
        return hasScholarship;
    }

    public void setHasScholarship(boolean hasScholarship) {
        this.hasScholarship = hasScholarship;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}
