package org.atp.lesson1.input;

import org.atp.lesson1.input.IntegerReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class IntegerBufferedReader implements IntegerReader {
    @Override
    public int[] getIntsFromConsole() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            int[] integers = new int[Integer.parseInt(bufferedReader.readLine())];
            for (int i = 0; i < integers.length; i++){
                integers[i] = Integer.parseInt(bufferedReader.readLine());
            }
            bufferedReader.close();
            return integers;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new int[0];
    }
}
