package org.atp.lesson1.input;

import org.atp.lesson1.input.IntegerReader;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IntegerScanner implements IntegerReader {

    public List<Integer> getAllIntegersFromConsole() {
        ArrayList<Integer> integers = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            integers.add(scanner.nextInt());
        }
        scanner.close();
        return integers;
    }

    @Override
    public int[] getIntsFromConsole() {
        Scanner scanner = new Scanner(System.in);
        int[] ints = new int[scanner.nextInt()];
        for (int i = 0; i < ints.length; i++) {
            ints[i] = scanner.nextInt();
        }
        scanner.close();
        return ints;
    }
}
