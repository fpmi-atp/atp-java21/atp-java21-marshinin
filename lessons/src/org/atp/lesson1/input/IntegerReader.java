package org.atp.lesson1.input;

public interface IntegerReader {
    /**
     * Returns integers from console.
     * @return List with integers.
     */
    int[] getIntsFromConsole();
}
