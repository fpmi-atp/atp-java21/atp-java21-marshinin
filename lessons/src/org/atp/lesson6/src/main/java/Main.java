import dao.PersonDAO;
import dto.Person;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.usermodel.*;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;

public class Main {
    public static void main(String[] args) throws IOException, IllegalAccessException {
        String filename = "report" + System.currentTimeMillis() + ".xlsx";
        FileOutputStream fileOutputStream = new FileOutputStream(filename);

        //region init
        XSSFWorkbook report = new XSSFWorkbook();
        XSSFSheet sheet = report.createSheet();
        XSSFRow row = sheet.createRow(0);
        XSSFCell cell;
        //endregion

        Field[] fields = Person.class.getDeclaredFields();

        //region header
        for (int i = 0; i < fields.length; i++) {
            cell = row.createCell(i);

            //region style
            XSSFCellStyle style = report.createCellStyle();
            style.setAlignment(HorizontalAlignment.CENTER);
            style.setFillBackgroundColor((short) 123);
            cell.setCellStyle(style);
            //endregion

            cell.setCellValue(fields[i].getName());
            sheet.createFreezePane(0, 1);
        }
        //endregion

        for (Field field : fields) {
            field.setAccessible(true);
        }

        int count = 1;

        //region insert data
        for (Person person : new PersonDAO().getAllPeople()) {
            row = sheet.createRow(count);

            for (int i = 0; i < fields.length; i++) {
                cell = row.createCell(i);
                if (fields[i].getType() == String.class) {
                    cell.setCellValue((String) fields[i].get(person));
                }
                else if (fields[i].getType() == int.class) {
                    cell.setCellValue((Integer) fields[i].get(person));
                }
            }

            count++;
        }
        //endregion

        report.write(fileOutputStream);
    }
}
