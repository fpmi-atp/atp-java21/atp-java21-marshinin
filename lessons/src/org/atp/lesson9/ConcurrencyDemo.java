package org.atp.lesson9;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.IntStream;

public class ConcurrencyDemo {
    public static void main(String[] args) throws InterruptedException {
        Service service = new Service();
        final ExecutorService pool = Executors.newFixedThreadPool(1_000);

        IntStream.range(0, 1_000_000).forEach(i -> pool.execute(service::inc));

        pool.shutdown();
        pool.awaitTermination(1, TimeUnit.MINUTES);
        System.out.println(service.getState());
    }
}

class Service {
    private int state = 0;
    private AtomicInteger ai = new AtomicInteger();
    //    private Collection collection = Collections.synchronizedList(new ArrayList<>());
    private Object monitor = new Object();
    private ReadWriteLock lock = new ReentrantReadWriteLock();

    /**
     * 1. read
     * 2. change
     * 3. save
     */
    public void inc() {
//        synchronized (monitor) {
//            state++;
//        }
//        lock.readLock().lock();
        try {
            lock.writeLock().lock();
            //...
            //...
        } finally {
            lock.writeLock().unlock();
        }
    }

    public int getState() {
        synchronized (monitor) {
            return state;
        }

//        try {
//            lock.readLock().lock();
//        } finally {
//            lock.readLock().unlock();
//        }
    }
}

class Worker extends Thread {
    private /*volatile*/ boolean stop;

    public synchronized void setStop(boolean stop) {
        this.stop = stop;
    }

    @Override
    public void run() {
//        if(!stop) while (true){}

        while (!stop) {
            synchronized (this) {

            }
        }
    }
}

class WorkerDemo {
    public static void main(String[] args) throws InterruptedException {
        Worker worker = new Worker();
        worker.start();
        Thread.sleep(1_000);
        worker.setStop(true);
    }
}
