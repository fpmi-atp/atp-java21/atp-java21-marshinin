package org.atp.lesson7;

import org.atp.lesson7.reflection.Car;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class Main {
    public static void main(String[] args) {
        //region Class init
        try {
            Class<?> carClass = Class.forName("org.atp.lesson7.reflection.Car");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Car car = new Car();
        Class<? extends Car> carClass = car.getClass();

        Class<Car> clazz = Car.class;
        //endregion

        //region fields
        Field[] declaredFields = carClass.getDeclaredFields();
        for (Field field : declaredFields) {
//            System.out.println(field);
        }

        try {
            Field field = carClass.getDeclaredField("horsePower");
//            System.out.println(field);

//            Field badField = carClass.getDeclaredField("asdarsgf");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

//        Field[] fields = carClass.getFields();
//        for (Field field : fields) {
//            System.out.println(field);
//        }

//        try {
//            Field field = carClass.getField("serialNumber");
//            System.out.println(field);
//
//            Field badField = carClass.getField("horsePower");
//        } catch (NoSuchFieldException e) {
//            e.printStackTrace();
//        }
        //endregion

        //region methods
//        Method[] methods = carClass.getDeclaredMethods();
//        Method[] methods = carClass.getMethods();
//        for (Method method : methods) {
//            System.out.println(method);
//        }

        try {
            Method printMethod = carClass.getDeclaredMethod("printSerialNumber");
//            System.out.println(printMethod);

            Method setHorseMethod = carClass.getDeclaredMethod("setHorsePower", int.class);
//            System.out.println(setHorseMethod);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        Car newCar = new Car() {
            @Override
            protected void printSerialNumber() {

            }
        };
        Method method = newCar.getClass().getEnclosingMethod();
//        System.out.println(method);
        //endregion

        //region fieldMagic
        Car car1 = new Car(100, "123");
        try {
            Field field = carClass.getDeclaredField("horsePower");
            field.setAccessible(true);

            String name = field.getName();
            Class type = field.getType();
            int modifier = field.getModifiers();

            System.out.println(name);
            System.out.println(type);
            System.out.println(modifier);
            System.out.println(Modifier.isPrivate(modifier));
            System.out.println(Modifier.isFinal(modifier));
//            int value = (int) field.get(car1);
//            System.out.println(value);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        //endregion
    }
}